#include <pthread.h>
#include <time.h>

#include "libary/msg.h"

#define REENTRANT

extern key_t key;
extern int msqid;

void *send(){

    char *tekst;
    while(true){
        scanf("%s",tekst);

        if(add_msg(SERVER,getpid(),tekst)) {
            printf("CLIENT : sent %s\n",tekst);
        } else {
            printf("Queue is nearly full - didnt send anything\n");
            // to consider cuz when read msg then it's dissapear from queue
        }
    }
        pthread_exit((void*)10);
}

void *recieve(){

    while(true){
        get_msg(getpid());
        printf("CLIENT : recived %s\n",buf_two.mtext);
    }

        pthread_exit((void*)15);
}

int main(){
    pthread_t thread_1;
    pthread_t thread_2;

    void* status_1;
    void* status_2;

    get_key();
    open_queue();

    if(pthread_create(&thread_1,NULL,send,NULL)){
        printf("pthread_create error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_create(&thread_2,NULL,recieve,NULL)){
        printf("pthread_create error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_join(thread_1,&status_1)){
        printf("pthread_join error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_join(thread_2,&status_2)){
        printf("pthread_join error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_detach(thread_1) == -1){
        printf("pthread_detach error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_detach(thread_2) == -1){
        printf("pthread_detach error!\n");
        exit(EXIT_FAILURE);
    }

    printf("Exit code thread_1 : %d\n",status_1);
    printf("Exit code thread_2 : %d\n",status_2);

	return 0;
}