/*
	Proces klient wysyła do procesu serwera ciąg znaków. Serwer odbiera ten ciąg znaków i przetwarza go
	zmieniając w nim wszystkie litery na duze, a następnie wysyła tak przetworzony ciąg znaków z powrotem do
	klienta. Klient odbiera przetworzony ciąg znaków i wypisuje go na ekranie.
	Posługując się mechanizmem kolejki komunikatów, nalezy zaimplementować powyzsze zadanie typu klient-serwer
	z mozliwością obsługi wielu klientów jednocześnie. W rozwiązaniu uzyć jednej kolejki komunikatów.
	Zastosować odpowiednie etykietowanie komunikatów w celu rozrozniania w kolejce danych dla serwera oraz
	danych dla poszczególnych klientów. 
*/

#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>
#include <string.h>

#include <sys/msg.h>
#include <sys/types.h>
#include <sys/ipc.h>

#define MSGSZ     128

#define SERVER 1337

#define true 1
#define TRUE 1
#define false 0
#define FALSE 0

void convertToUpperCase(char*);

key_t key;
int msqid;

// int msgget(key_t key, int msgflg);  - odwolywanie sie do kolejki komunikatow (zwraca id msgid)
// int msgsnd( int msqid, const void *msg_ptr, size_t msg_sz, int msgflg); - dodanie komunikatu do kolejk 
// int msgrcv( int msqid, void *msg_ptr, size_t msg_sz, long int msgtype, int msgflg); - pobranie komunikatu z kolejki
// int msgctl(int msqid, int cmd, struct msqid_ds *buf );  - obsługa kolejek komunikatów


struct message{
	long int mtype;
	long int id;
	char mtext[MSGSZ];
} buf_one, buf_two;

void get_key(){	
    key = ftok("/home/dliszka/C/SYSTEMY/client-server-issue",'A');
	if(key == -1){
		perror("ftok error");
		exit(EXIT_FAILURE);
	}
}

void open_queue(){
	msqid = msgget(key,0600 | IPC_CREAT);
	if(msqid == -1){
		perror("msgget error");
		exit(EXIT_FAILURE);
	}
}

void add_msg(long int type, long int id,char* tekst){

	buf_one.mtype = type;
	buf_one.id = id;

	strcpy(buf_one.mtext, tekst);
	void* msg_ptr = &buf_one;

	// check if msg is going to be sent to SERVER
	// if yes then check if the queue is nearly full
	// if yes then do not send msg
	// if no then send msg

	if ( msgsnd(msqid,msg_ptr,sizeof(long)+MSGSZ*sizeof(char), 0) == -1 ){
		perror("msgsnd error");
		exit(EXIT_FAILURE);
	}	


	/*
		możliwe flagi w msgflg (4 arg): 

		IPC_NOWAIT – funkcja powróci bez wysłania komunikatu zwracając -1;
		IPC_WAIT – proces zostanie zawieszony w oczekiwaniu na zwolnienie miejsca w
		kolejce. 
	*/
}

struct message* get_msg(long int type){
	int bytes = 0;

	void* msg_ptr = &buf_two;

	// IF IPC_NOWAIT - THEN AFTER WE CHECK QUEQUE AND NOT FOUND A MSG WITH THIS TYPE
	// MSGRCV WILL RETURN ERROR (ENOMSG)

	/* IF FLAG IS SET TO 0 THEN MSGRCV WILL WAIT UNTIL :
		- A message of the desired type is placed on the queue
		- The message queue identifier msqid is removed from the system; when this occurs, errno is set equal to [EIDRM] and -1 is returned.
		- The calling thread receives a signal that is to be caught; in this case a message is not received and the calling thread resumes execution in the manner prescribed in sigaction().
	*/

	bytes = msgrcv(msqid,msg_ptr,sizeof(long)+MSGSZ*sizeof(char),type,0);
	if(bytes == -1){
		perror("msgrcv error");
		exit(EXIT_FAILURE);
	}

	return &buf_two;
};

void delete_queue(){
	struct msqid_ds buf;

	 if ( msgctl(msqid,IPC_RMID,&buf ) == -1 ){
	 	perror("msgctl error");
	 	exit(EXIT_FAILURE);
	 }
}

void convertToUpperCase(char sPtr[]){
	int i=0;
	while(sPtr[i] != '\0'){
		if(sPtr[i] >= 97)
			if(sPtr[i]<=122)
				sPtr[i] = sPtr[i] - 32;
		++i;
	}
}