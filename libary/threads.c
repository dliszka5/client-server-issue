#include <pthread.h>

#include <stdio.h>
#include <time.h>
#include <stdlib.h>


#define REENTRANT

void *fun_1(){
    printf("TID(fun_1) : %lu\n",pthread_self());
        pthread_exit((void*)10);
}

void *fun_2(){
    printf("TID(fun_2) : %lu\n",pthread_self());
        pthread_exit((void*)15);
}

int main(){
    pthread_t thread_1;
    pthread_t thread_2;

    void* status_1;
    void* status_2;

    printf("TID(main) : %lu\n",pthread_self());

    if(pthread_create(&thread_1,NULL,fun_1,NULL)){
        printf("pthread_create error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_create(&thread_2,NULL,fun_2,NULL)){
        printf("pthread_create error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_join(thread_1,&status_1)){
        printf("pthread_join error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_join(thread_2,&status_2)){
        printf("pthread_join error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_detach(thread_1) == -1){
        printf("pthread_detach error!\n");
        exit(EXIT_FAILURE);
    }

    if(pthread_detach(thread_2) == -1){
        printf("pthread_detach error!\n");
        exit(EXIT_FAILURE);
    }

    printf("Exit code thread_1 : %d\n",status_1);
    printf("Exit code thread_2 : %d\n",status_2);

    return 0;

}