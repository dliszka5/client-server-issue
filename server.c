#include <stdio.h>

#include "libary/msg.h"

#include  <signal.h>

extern key_t key;
extern int msqid;

void interrupt_handler(int);

int main(){

	get_key();
	open_queue();

	signal(SIGINT,interrupt_handler);

	while(true){
		get_msg(SERVER);

		printf("SERVER : recived : %s\n",buf_two.mtext);
		char* napis = buf_two.mtext;
		long int id = buf_two.id;
			convertToUpperCase(napis);

		// server must check if process with that pid is still working
		if( getpgid(id) == -1){
			if(errno == ESRCH){
				printf("Process that sent this data has been closed\n");
			}
		} else {
			printf("SERVER : sending : %s\n",napis);
			add_msg(id,SERVER,napis);
		}

	}
	
	delete_queue();



	return 0;
}

void interrupt_handler(int signal){	
	if (signal == SIGINT) {
		delete_queue();
			printf("Queue deleted");
	}
		exit(0);
}